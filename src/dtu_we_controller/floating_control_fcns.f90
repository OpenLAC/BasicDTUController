module floating_control_fcns_mod
	!
	! Module contains functions for floating controller used by DTU Wind Energy Controller
	!
	use user_defined_types
	use global_variables
	use misc_mod
	implicit none
!**************************************************************************************************
    contains
!**************************************************************************************************
! (TO-DO)function tower_velocity_calcuation()
!
! function to compute tower top velocity to rotor speed gain
    function floating_control_kgain_TTfaVel_to_pitch(Floatingvar, PitchGSVar, dqa_dtheta0,PitchMeanFilt)
    implicit none
    ! input and output variables
    real(mk)  dqa_dtheta0, PitchMeanFilt
	type(TFloatingvar) Floatingvar
    type(TPitchGSvar)  PitchGSVar
    real(mk) :: floating_control_kgain_TTfaVel_to_pitch

    ! local variables
	real(mk) :: kgain_GenTorque, dqa_dtheta, GSvar, GSvalue

	! main
	GSvar = Floatingvar%GSvar
	GSvalue = 1+Floatingvar%KK1_tq*GSvar + Floatingvar%KK2_tq*GSvar**2
	kgain_GenTorque = Floatingvar%TGgain *GSvalue
    dqa_dtheta = dqa_dtheta0/(1.0_mk + PitchGSVar%invkk1*PitchMeanFilt + PitchGSVar%invkk2*PitchMeanFilt**2)
    floating_control_kgain_TTfaVel_to_pitch= kgain_GenTorque /dqa_dtheta /1.0_mk
    end function
!
subroutine floating_control_additional_pitch(Floatingvar, PitchGSVar, TTfa_vel, wsp, dqa_dtheta0, PitchMeanFilt, dump_array)
	implicit none
	real(mk), intent(in) :: TTfa_vel, wsp, PitchMeanFilt,dqa_dtheta0
    type(TPitchGSvar), intent(in) :: PitchGSVar
	type(TFloatingvar), intent(inout) :: Floatingvar
	real(mk), intent(in) :: dump_array(50)

    ! local varabiles
	real(mk) switchfactor, TTfa_vel_filt, y(2), WSPfilt, GSvar, kGain
	real(mk) :: region,kgain_TTfaVel_to_pitch
    ! save original rotor speed

    switchfactor = Floatingvar%switchfactor
 
	!******************
	! filtering tower top velocity signal
	!******************
	!! Low-pass filter
	if (float2orderlpfvar%f0 .gt. 0) then
		y = lowpass2orderfilt(deltat, stepno, float2orderlpfvar, TTfa_vel)   
	else
		y = TTfa_vel
	endif
	!! Band-pass filter 
	if (float2orderbpfvar%f0 .gt.0) then
		y = bandpassfilt(deltat, stepno, float2orderbpfvar, TTfa_vel)
	else 
		y = TTfa_vel
	endif
	TTfa_vel_filt = y(1)
	!******************
	! filterd wind speed for gain-scheduling
	!******************
	if (Floatingvar%GSmode .gt. 0.0_mk)  then
		! Floatingvar%GSvar = PitchColRef
		Floatingvar%GSvar = PitchMeanFilt
	else
		WSPfilt = lowpass1orderfilt(deltat, stepno, wspfirstordervar, wsp)
		Floatingvar%GSvar = abs(WSPfilt - Floatingvar%RatedWindSpeed)
	endif

	!****************** 
	! Main Loop: Tower-genTorq loop
	!****************** 
    ! Compute gain in order to modify the filtered rotor speed
	if ((abs(Floatingvar%TGgain) .gt. 0.0_mk)) then
		region = switchfactor*dump_array(10)
        kgain_TTfaVel_to_pitch = floating_control_kgain_TTfaVel_to_pitch(Floatingvar, PitchGSVar, dqa_dtheta0,PitchMeanFilt)
        Floatingvar%Pitch_addition  =  kgain_TTfaVel_to_pitch * TTfa_vel_filt * region
	endif
	! for output
	Floatingvar%switchfactor = switchfactor
	Floatingvar%kPitchAddition = kgain_TTfaVel_to_pitch
	Floatingvar%TTfa_vel_filt = TTfa_vel_filt
end subroutine floating_control_additional_pitch
!
end module floating_control_fcns_mod
