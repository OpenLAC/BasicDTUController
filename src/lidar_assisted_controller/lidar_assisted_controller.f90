module lidar_assisted_controller_mod
use lidar_assisted_controller_fcns_mod
implicit none
contains
!**************************************************************************************************
! ****** LiDAR-assisted Controller
!**************************************************************************************************
subroutine init_lidar_assisted_controller(array1,array2) bind(c,name='init_lidar_assisted_controller')
	implicit none
      !DEC$ IF .NOT. DEFINED(__LINUX__)
      !DEC$ ATTRIBUTES DLLEXPORT :: init_lidar_assisted_controller
      !GCC$ ATTRIBUTES DLLEXPORT :: init_lidar_assisted_controller
      !DEC$ END IF
	real*8 :: array1(10),array2(1)
	logical :: findes
	integer :: i, ifejl
	character(len=32) text32
   ! Input parameters
   ! constant 1 ; on-off switch [-] [0: off, 1: on ]
   ! constant 2 ; Time to be active [s]
   ! constant 3 ; Time-delay [s]
   ! constant 4 ; Time constant of the first-order low-pass filter on LOS [s]
   ! constant 5 ; half-cone openning angle [deg]
   ! constant 6 ; Feed-forward pitch rate limit [deg/s]
   ! constant 7 ; Number of LiDAR beams [-]
   ! constant 8 ; Rotor tilt angle [deg]

	lac_onoff = array1(1)
	active_time = array1(2)
	if (lac_onoff .gt. 0) then
		write(6,*) "*** 		LiDAR-assised Controller is activated ! 		***"
		active_time = array1(2)
		time_delay = array1(3)
		lac_wind1ordervar%tau = array1(4)
		op_angle = array1(5)
		switchingvar%tau = 2.5d0
		FF_PitchRate_limit = array1(6)*degrad
		No_of_Beams = array1(7)
		tilt_angle = array1(8)
	endif
   ! Read steady-state pitch vs wind speed table
   inquire(file='./control/ssdata.dat',exist=findes)
   if (findes) then
       open(88,file='./control/ssdata.dat')
       read(88,*,iostat=ifejl) SSdatavar%lines
       if (ifejl.eq.0) then
         do i=1,SSdatavar%lines
           read(88,*,iostat=ifejl) SSdatavar%wpdata(i,1),SSdatavar%wpdata(i,2)
           if (ifejl.ne.0) then
             write(6,*) ' *** ERROR *** Could not read lines in steady state pitch angle vs wind speed table in ssdata.dat.'
             stop
           endif
           SSdatavar%wpdata(i,2)=SSdatavar%wpdata(i,2)*degrad
         enddo
       else
         write(6,*) ' *** ERROR *** Could not read number of lines in steady state pitch angle vs wind speed table in ssdata.dat'
         stop
       endif
       close(88)
    else
       write(6,*) ' *** ERROR *** File ''ssdata.dat (steady state pitch vs wind speeed) does not exist in the ./control/ folder'
       stop
    endif
return 
end subroutine init_lidar_assisted_controller
 !**************************************************************************************************
subroutine update_lidar_assisted_controller(array1,array2) bind(c,name='update_lidar_assisted_controller')
	implicit none
    !DEC$ IF .NOT. DEFINED(__LINUX__)
    !DEC$ ATTRIBUTES DLLEXPORT :: update_lidar_assisted_controller
    !GCC$ ATTRIBUTES DLLEXPORT :: update_lidar_assisted_controller
    !DEC$ END IF

	real*8 :: array1(25),array2(10)
! Input array1 must contains
!
!    1:  General time [s]     
!    2:  LOS1  [m/s]
!    ...
!        LOS_n [m/s]
!
! Output array2 contains
!
!   1:  Feed-forward pitch rate  [rad/s]
!   2:  Average LOS [m/s]
!   3:  Filtered averaged LOS [m/s]
!   4:  Time-delayed and filtered averaged LOS [m/s]
!   5:  Switching factor for the algorithm [-]
!   6:  Feed-forward pitch angle [rad]
!
! Local variables
	real*8 :: time = 0.0d0, time_old = 0.0d0, deltat = 0.01d0
	real*8 :: avgV = 0.0d0, avgV_filt =0.0d0
	real*8 :: FFPitchRate = 0.0d0, FFPitchAngle_old = 0.0d0 , FFPitchAngle = 0.0d0
	real*8 :: lac_switchfactor = 0.0d0, lac_switchfactor_lp = 0.0d0
	integer :: i = 1
	
	time = array1(1)
	if (time .gt. time_old) then
		 deltat = time - time_old
		 time_old = time
		 stepno = stepno + 1
	endif
	if (stepno.eq.1) then
		idx = int(time_delay/deltat)+1
		curr_idx = 1
	endif

	! calculating time-delayed and filtered averaged LOS
	avgV = 0.0d0
	do i = 1, No_of_Beams
	 avgV = avgV + array1((i-1)*5+2)/cos((op_angle+tilt_angle)*pi/180.0d0) 
	end do
	avgV = avgV/dble(No_of_Beams)
	avgV_filt =  lowpass1orderfilt(deltat, stepno, lac_wind1ordervar, avgV)
	wind_array(idx) = avgV_filt ! store filtered wind speed into an array
	
	! calculate feed-forward pitch angle and rate
	FFPitchAngle_old = FFPitchAngle
	FFPitchAngle = GetSSPitch(wind_array(curr_idx))
	FFPitchRate = min((FFPitchAngle - FFPitchAngle_old)/deltat, FF_PitchRate_limit)
	 

	! switching factor
	if ((time .gt. active_time)) then 
			lac_switchfactor=1.0d0
		else
			lac_switchfactor=0.0d0
		endif
	lac_switchfactor_lp=lowpass1orderfilt(deltat,stepno,lac_switchingvar,lac_switchfactor) 

	! shifting the deley index by 1 for the next sampling time
	idx = idx + 1
	curr_idx = curr_idx + 1
	if (idx .gt. max_array) then
		idx = 1
	endif
	if (curr_idx .gt. max_array) then
		curr_idx = 1
	endif

   ! output
   array2(1) = FFPitchRate*lac_switchfactor_lp		!   1:  Feed-forward pitch rate  [rad/s]
   array2(2) = avgV        							!   2:  Average LOS [m/s]
   array2(3) = avgV_filt 							!   3:  Filtered averaged LOS [m/s]
   array2(4) = wind_array(curr_idx) 				!   4:  Time-delayed and filtered averaged LOS [m/s]
   array2(5) = lac_switchfactor_lp 					!   5:  Switching factor for the algorithm [-]
   array2(6) = FFPitchAngle         				!   6:  Feed-forward pitch angle [rad]
return
end subroutine update_lidar_assisted_controller
!***********************
end module lidar_assisted_controller_mod
