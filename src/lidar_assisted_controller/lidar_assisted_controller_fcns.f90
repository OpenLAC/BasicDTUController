module lidar_assisted_controller_fcns_mod
use misc_mod
use dtu_we_controller_fcns
implicit none
integer*4 :: lac_onoff = 0
type(Twpdata),        save :: SSdatavar
real*8 :: FF_PitchRate_limit = 10.0d0
integer,parameter :: max_array = 5000 ! maximum array number
real*8 :: wind_array(max_array)
integer :: idx=1 , curr_idx=1
real*8 :: active_time = 10.0d0
real*8 :: time_delay
real*8 :: op_angle = 18.0d0 ! half-cone openning angle [deg]
integer :: No_of_Beams = 4
real*8 :: tilt_angle = 5.0d0 

type(Tfirstordervar), save :: lac_wind1ordervar
type(Tfirstordervar), save:: lac_switchingvar
!**************************************************************************************************
contains
!**************************************************************************************************
!
function GetSSPitch(wsp)
   ! Computes pitch angle from look-up table based on wind speed input
   real(mk) GetSSPitch,wsp
   ! local vars
   real(mk) x, x0, x1, f0, f1, pitch
   integer i
   i=1
   do while((SSdatavar%wpdata(i, 1) .le. wsp) .and. (i .le. SSdatavar%lines))
      i=i+1
   enddo
   if (i.eq.1) then
      GetSSPitch = SSdatavar%wpdata(1, 2)
   elseif (i .gt. SSdatavar%lines) then
      GetSSPitch = SSdatavar%wpdata(SSdatavar%lines, 2)
   else
      x = wsp
      x0 = SSdatavar%wpdata(i-1, 1)
      x1 = SSdatavar%wpdata(i, 1)
      f0 = SSdatavar%wpdata(i-1, 2)
      f1 = SSdatavar%wpdata(i, 2)
      pitch = interpolate(x, x0, x1, f0, f1)
      GetSSPitch = pitch
   endif
   return
end function GetSSPitch
! *************************************************************
end module lidar_assisted_controller_fcns_mod